﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCP_Sockets_Server
{
    class ChatUser
    {
        public string Username { get; set; }
        public TcpClient Client { get; set; }
        public StreamReader Reader { get; set; }
        public StreamWriter Writer { get; set; }

        public ChatUser(TcpClient client, string username)
        {
            Client = client;
            Username = username;
            Reader = new StreamReader(client.GetStream());
            Writer = new StreamWriter(client.GetStream());
            Writer.AutoFlush = true;
        }
    }

    class Server
    {
        static List<ChatUser> chatUsers = new List<ChatUser>();

        static void Main(string[] args)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, 8080);
            listener.Start();

            Console.WriteLine("Server started...");

            while (true)
            {
                var client = listener.AcceptTcpClient();
                ListenClient(client);
            }
        }

        static public void ListenClient(TcpClient client)
        {
            Task.Run(() =>
            {
                string username = null;
                bool connected = true;
                var reader = new StreamReader(client.GetStream());
                var writer = new StreamWriter(client.GetStream());
                writer.AutoFlush = true;

                while (connected)
                {
                    var data = reader.ReadLine(); //connect:Iskender
                    var command = data.Split(':')[0];
                    var message = data.Split(':')[1];
                    switch (command)
                    {
                        case "connect":
                            username = message;
                            Console.WriteLine($"{username} connected!");
                            chatUsers.Add(new ChatUser(client, username));
                            Console.WriteLine($"{chatUsers.Count} users connected");
                            break;
                        case "message":
                            Console.WriteLine($"{username}: {message}");
                            BroadcastMessage(username, message);
                            break;
                        case "end":
                            Console.WriteLine($"{username} disconnected!");
                            connected = false;
                            chatUsers.RemoveAll(x => x.Username == username);
                            Console.WriteLine($"{chatUsers.Count} users connected");
                            break;
                    }
                }

                reader.Close();
                writer.Close();
                client.Close();
            });
        }

        static public void BroadcastMessage(string from, string messasge)
        {
            foreach (var user in chatUsers)
            {
                if (user.Username != from)
                {
                    user.Writer.WriteLine($"{from}: {messasge}");
                }
            }
        }
    }
}
