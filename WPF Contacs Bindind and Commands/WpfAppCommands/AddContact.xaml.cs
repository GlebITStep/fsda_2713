﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppCommands
{
    public partial class AddContact : Window
    {
        public AddContactData Data { get; set; }

        public AddContact(Contact contact = null)
        {
            InitializeComponent();
            Data = new AddContactData();
            DataContext = Data;
            if (contact != null)
            {
                Data.Contact = contact;
                Data.Action = "Edit";
            }
        }

        private void AddClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }

    public class AddContactData
    {
        public Contact Contact { get; set; } = new Contact();
        public string Action { get; set; } = "Add";
    }
}
