﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppCommands
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowData Data { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Data = new MainWindowData();
            DataContext = Data;
            AddContacts();
        }

        void AddContacts()
        {
            Data.List.Add(new Contact { Name = "Gleb", Phone = "123" });
            Data.List.Add(new Contact { Name = "Mustafa", Phone = "456" });
        }

        private void ButtonAddClick(object sender, RoutedEventArgs e)
        {
            var window = new AddContact();
            window.ShowDialog();
            var contact = window.Data.Contact;
            Data.List.Add(contact);
        }

        private void ButtonEditClick(object sender, RoutedEventArgs e)
        {
            var window = new AddContact(Data.Selected);
            window.ShowDialog();
        }

        private void ButtonRenameClick(object sender, RoutedEventArgs e)
        {
            Data.Selected.Name = "Iskender";
            Data.Selected.Phone = "999";
            MessageBox.Show(Data.Selected.Name);
        }
    }

    public class MainWindowData : ObservableObject
    {
        private ObservableCollection<Contact> list = new ObservableCollection<Contact>();
        public ObservableCollection<Contact> List
        {
            get => list;
            set { list = value; OnPropertyChanged(); }
        }

        private Contact selected;
        public Contact Selected
        {
            get => selected;
            set { selected = value; OnPropertyChanged(); }
        }
    }
}
