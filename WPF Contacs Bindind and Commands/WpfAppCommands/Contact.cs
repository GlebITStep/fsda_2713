﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppCommands
{
    public class Contact : ObservableObject
    {
        private string name;
        public string Name
        {
            get => name;
            set { name = value; OnPropertyChanged(); } 
        }

        private string phone;
        public string Phone
        {
            get => phone;
            set { phone = value; OnPropertyChanged(); }
        }
    }
}
