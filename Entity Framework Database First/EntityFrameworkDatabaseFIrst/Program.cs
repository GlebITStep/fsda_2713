﻿using EntityFrameworkDatabaseFIrst.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkDatabaseFIrst
{
    class Program
    {
        //static bool CheckPrice(Product product)
        //{
        //    return product.UnitPrice < 10;
        //}

        static void Main(string[] args)
        {
            using (var db = new ShopEntities())
            {
                ////select * from Products
                //var result = db.Products;

                ////select * from Products where UnitPrice < 10
                //var result = db.Products.Where(x => x.UnitPrice < 10);

                //select * from Products where UnitPrice < 10 and IsDiscontinued == true
                //var result = db.Products.Where(x => x.UnitPrice < 10 && x.IsDiscontinued == true);
                //var result = db.Products
                //    .Where(x => x.UnitPrice < 10)
                //    .Where(x => x.IsDiscontinued == true);

                ////select * from Products order by UnitPrice
                //var result = db.Products.OrderBy(x => x.UnitPrice);

                ////select * from Products order by UnitPrice desc
                //var result = db.Products.OrderByDescending(x => x.UnitPrice);

                ////select * from Products where UnitPrice < 10 order by ProductName desc
                //var result = db.Products
                //    .Where(x => x.UnitPrice < 10)
                //    .OrderBy(x => x.ProductName);

                ////select top 5 * from Products order by UnitPrice desc
                //var result = db.Products
                //    .OrderByDescending(x => x.UnitPrice)
                //    .Take(5);


                //var result = db.Products
                //    .Where(x => x.UnitPrice == db.Products.Max(y => y.UnitPrice));

                //var result = db.Products
                //    .Where(x => x.UnitPrice > 50);
                //var coll = new ObservableCollection<Product>(result);


                //foreach (var item in coll)
                //{
                //    Console.WriteLine($"{item.Id} " +
                //        $"{item.ProductName} " +
                //        $"{item.UnitPrice} " +
                //        $"{item.IsDiscontinued} ");
                //}


                ////select max(UnitPrice) from Products
                //var result = db.Products.Max(x => x.UnitPrice);

                ////select max(UnitPrice) from Products
                //var result = db.Products.Average(x => x.UnitPrice);

                //Console.WriteLine(result);



                ////select ProductName from Products
                //var results = db.Products.Select(x => x.ProductName);
                //foreach (var item in results)
                //{
                //    Console.WriteLine(item);
                //}



                //var result = db.Products
                //    .Where(x => x.UnitPrice == 10)
                //    .FirstOrDefault();

                //if (result != null)
                //    Console.WriteLine(result.ProductName);
            }
        }
    }
}
