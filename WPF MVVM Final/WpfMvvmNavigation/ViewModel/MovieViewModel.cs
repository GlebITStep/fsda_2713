﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMvvmNavigation.Messages;
using WpfMvvmNavigation.Services;

namespace WpfMvvmNavigation.ViewModel
{
    class MovieViewModel : ViewModelBase
    {
        private string result;
        public string Result { get => result; set => Set(ref result, value); }

        private readonly INavigationService navigationService;


        public MovieViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;

            Messenger.Default.Register<MovieMessage>(this, param =>
            {
                Result = param.Movie.Title;
            });
        }

        private RelayCommand goBackCommand;
        public RelayCommand GoBackCommand
        {
            get => goBackCommand ?? (goBackCommand = new RelayCommand(
                () => navigationService.Navigate("Search")
            ));
        }
    }
}
