﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using WpfMvvmNavigation.Model;
using WpfMvvmNavigation.Services;
using System.Collections.ObjectModel;
using WpfMvvmNavigation.Messages;
using WpfMvvmNavigation.Services;

namespace WpfMvvmNavigation.ViewModel
{
    class SearchViewModel : ViewModelBase
    {
        private string search;
        public string Search { get => search; set => Set(ref search, value); }

        private ObservableCollection<Movie> movies = new ObservableCollection<Movie>();
        public ObservableCollection<Movie> Movies { get => movies; set => Set(ref movies, value); }

        private readonly IMovieSearch movieSearch;
        private readonly INavigationService navigationService;

        public SearchViewModel(
            IMovieSearch movieSearch, 
            INavigationService navigationService)
        {
            this.movieSearch = movieSearch;
            this.navigationService = navigationService;
        }

        private RelayCommand searchCommand;
        public RelayCommand SearchCommand
        {
            get => searchCommand ?? (searchCommand = new RelayCommand(
                () => Movies = new ObservableCollection<Movie>(movieSearch.Search(Search))
            ));
        }

        private RelayCommand<Movie> fullInfoCommand;
        public RelayCommand<Movie> FullInfoCommand
        {
            get => fullInfoCommand ?? (fullInfoCommand = new RelayCommand<Movie>(
                param =>
                {
                    var movie = movieSearch.FullInfo(param.Id);
                    Messenger.Default.Send(new MovieMessage { Movie = movie });
                    navigationService.Navigate("Movie");
                }
            ));
        }
    }
}
