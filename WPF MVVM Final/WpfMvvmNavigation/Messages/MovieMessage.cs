﻿using WpfMvvmNavigation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvmNavigation.Messages
{
    class MovieMessage
    {
        public Movie Movie { get; set; }
    }
}
