﻿using WpfMvvmNavigation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvmNavigation.Services
{
    interface IMovieSearch
    {
        IEnumerable<Movie> Search(string title);
        Movie FullInfo(string imdbId);
    }
}
