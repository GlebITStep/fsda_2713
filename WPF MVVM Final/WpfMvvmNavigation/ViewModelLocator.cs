﻿using WpfMvvmNavigation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMvvmNavigation.ViewModel;
using GalaSoft.MvvmLight;

namespace WpfMvvmNavigation
{
    class ViewModelLocator
    {
        AppViewModel appViewModel;
        SearchViewModel searchViewModel;
        MovieViewModel movieViewModel;

        INavigationService navigationService;

        public ViewModelLocator()
        {
            navigationService = new NavigationService();

            appViewModel = new AppViewModel();
            searchViewModel = new SearchViewModel(new MovieSearch(), navigationService);
            movieViewModel = new MovieViewModel(navigationService);

            navigationService.AddPage("Movie", movieViewModel);
            navigationService.AddPage("Search", searchViewModel);

            navigationService.Navigate("Search");
        }

        public ViewModelBase GetMainViewModel()
        {
            return appViewModel;
        }
    }
}
