﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMvvmNavigation.Messages;

namespace WpfMvvmNavigation.ViewModel
{
    class SearchViewModel : ViewModelBase
    {
        private string search;
        public string Search { get => search; set => Set(ref search, value); }

        private RelayCommand searchCommand;
        public RelayCommand SearchCommand
        {
            get => searchCommand ?? (searchCommand = new RelayCommand(
                () => 
                {
                    Messenger.Default.Send(new SearchMessage { Text = Search });
                    Messenger.Default.Send(new PageMessage { Name = "Movie" });
                }
            ));
        }
    }
}
