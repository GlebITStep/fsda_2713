﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfMvvmNavigation.Messages;

namespace WpfMvvmNavigation.ViewModel
{
    class AppViewModel : ViewModelBase
    {
        private ViewModelBase currentPage;
        public ViewModelBase CurrentPage { get => currentPage; set => Set(ref currentPage, value); }

        SearchViewModel SearchViewModel = new SearchViewModel();
        MovieViewModel MovieViewModel = new MovieViewModel();

        Dictionary<string, ViewModelBase> pages = new Dictionary<string, ViewModelBase>();

        public AppViewModel()
        {
            pages.Add("Search", SearchViewModel);
            pages.Add("Movie", MovieViewModel);

            CurrentPage = pages["Search"];

            Messenger.Default.Register<PageMessage>(this, param =>
            {
                CurrentPage = pages[param.Name];
            });
        }

        //private RelayCommand<string> pageCommand;
        //public RelayCommand<string> PageCommand
        //{
        //    get => pageCommand ?? (pageCommand = new RelayCommand<string>(
        //        param => CurrentPage = pages[param]
        //    ));
        //}
    }
}
