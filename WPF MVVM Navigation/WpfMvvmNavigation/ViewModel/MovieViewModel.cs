﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMvvmNavigation.Messages;

namespace WpfMvvmNavigation.ViewModel
{
    class MovieViewModel : ViewModelBase
    {
        private string result;
        public string Result { get => result; set => Set(ref result, value); }

        public MovieViewModel()
        {
            Messenger.Default.Register<SearchMessage>(this, param =>
            {
                Result = param.Text;
            });
        }

        private RelayCommand goBackCommand;
        public RelayCommand GoBackCommand
        {
            get => goBackCommand ?? (goBackCommand = new RelayCommand(
                () => Messenger.Default.Send(new PageMessage { Name = "Search" })
            ));
        }
    }
}
