﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WeatherAsync
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        bool check;
        Mutex mutex;
        Semaphore semaphore;

        protected override void OnStartup(StartupEventArgs e)
        {
            //string guid = Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString();
            //mutex = new Mutex(true, guid, out check);
            //if (!check)
            //{
            //    MessageBox.Show("Application is already running!");
            //    Environment.Exit(0);
            //}

            string guid = Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString();
            semaphore = new Semaphore(2, 2, guid);
            semaphore.WaitOne();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            semaphore.Release();
            base.OnExit(e);
        }
    }
}
