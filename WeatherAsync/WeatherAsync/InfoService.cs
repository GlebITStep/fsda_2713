﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WeatherAsync.Model;


//https://app.quicktype.io/#l=cs&r=json2csharp
namespace WeatherAsync
{
    class InfoService
    {
        string ipGeoKey = "8b630bdaffc04c57996f6cb6f0552f28";
        string weatherApiKey = "835492c0eab300994ec658dfb16ad305";
        WebClient webClient = new WebClient();

        public Task<IpInfo> FindIpInfoAsync()
        {
            return Task.Run(() =>
            {
                var query = "https://api.myip.com/";
                var json = webClient.DownloadString(query);
                IpInfo data = JsonConvert.DeserializeObject<IpInfo>(json);
                return data;
            });
        }

        public Task<LocationInfo> FindLocationInfoAsync(string ip)
        {
            return Task.Run(() =>
            {
                var query = $"https://api.ipgeolocation.io/ipgeo?apiKey={ipGeoKey}&ip={ip}";
                var json = webClient.DownloadString(query);
                LocationInfo data = JsonConvert.DeserializeObject<LocationInfo>(json);
                return data;
            });
        }

        public Task<WeatherInfo> FindWeatherInfoAsync(string city)
        {
            return Task.Run(() =>
            {
                WebClient webClient = new WebClient();
                var query = $"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={weatherApiKey}&units=metric";
                var json = webClient.DownloadString(query);
                WeatherInfo data = JsonConvert.DeserializeObject<WeatherInfo>(json);
                return data;
            });
        }

        public Task<CountryInfo> FindCountryInfoAsync(string country)
        {
            return Task.Run(() =>
            {
                WebClient webClient = new WebClient();
                var query = $"https://restcountries.eu/rest/v2/name/{country}";
                var json = webClient.DownloadString(query);
                CountryInfo[] data = JsonConvert.DeserializeObject<CountryInfo[]>(json);
                return data[0];
            });
        }

        public string FindFlagImage(string countryCode)
        {
            return $"https://www.countryflags.io/{countryCode}/shiny/64.png";
        }
    }
}
