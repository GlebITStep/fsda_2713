﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WeatherAsync.Model;

namespace WeatherAsync
{
    class ViewModel : ViewModelBase
    {
        private string ip;
        public string Ip { get => ip; set => Set(ref ip, value); }

        private string country;
        public string Country { get => country; set => Set(ref country, value); }

        private string region;
        public string Region { get => region; set => Set(ref region, value); }

        private string city;
        public string City { get => city; set => Set(ref city, value); }

        private string temp;
        public string Temp { get => temp; set => Set(ref temp, value); }

        private string currency;
        public string Currency { get => currency; set => Set(ref currency, value); }

        private string image;
        public string Image { get => image; set => Set(ref image, value); }

        private string code;
        public string Code { get => code; set => Set(ref code, value); }

        private int progress;
        public int Progress { get => progress; set => Set(ref progress, value); }

        InfoService service;

        public ViewModel()
        {
            service = new InfoService();
        }

        //public async void Search()
        //{
        //    IpInfo ipInfo = await service.FindIpInfo();
        //    Ip = ipInfo.Ip;
        //    Country = ipInfo.Country;
        //    Code = ipInfo.Cc;
        //    Progress += 20;

        //    LocationInfo locationInfo = await service.FindLocationInfo(ipInfo.Ip);
        //    Region = locationInfo.StateProv;
        //    City = locationInfo.City;
        //    Progress += 20;

        //    WeatherInfo weatherInfo = await service.FindWeatherInfo(locationInfo.City);
        //    Temp = weatherInfo.Main.Temp.ToString();
        //    Progress += 20;

        //    CountryInfo countryInfo = await service.FindCountryInfo(ipInfo.Country);
        //    Currency = countryInfo.Currencies[0].Name;
        //    Progress += 20;

        //    Image = service.FindFlagImage(ipInfo.Cc);
        //    Progress += 20;
        //}

        private RelayCommand searchCommand;
        public RelayCommand SearchCommand
        {
            get => searchCommand ?? (searchCommand = new RelayCommand(
                async () =>
                {
                    IpInfo ipInfo = await service.FindIpInfoAsync();
                    Ip = ipInfo.Ip;
                    Country = ipInfo.Country;
                    Code = ipInfo.Cc;
                    Progress += 20;

                    LocationInfo locationInfo = await service.FindLocationInfoAsync(ipInfo.Ip);
                    Region = locationInfo.StateProv;
                    City = locationInfo.City;
                    Progress += 20;

                    WeatherInfo weatherInfo = await service.FindWeatherInfoAsync(locationInfo.City);
                    Temp = weatherInfo.Main.Temp.ToString();
                    Progress += 20;

                    CountryInfo countryInfo = await service.FindCountryInfoAsync(ipInfo.Country);
                    Currency = countryInfo.Currencies[0].Name;
                    Progress += 20;

                    Image = service.FindFlagImage(ipInfo.Cc);
                    Progress += 20;
                }
            ));
        }
    }
}
