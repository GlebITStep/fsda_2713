﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherAsync.Model
{
    class IpInfo
    {
        public string Ip { get; set; }
        public string Country { get; set; }
        public string CC { get; set; }
    }
}
