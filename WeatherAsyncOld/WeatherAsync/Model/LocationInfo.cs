﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherAsync.Model
{
    class LocationInfo
    {
        public string City { get; set; }
        public string State_prov { get; set; }
    }
}
