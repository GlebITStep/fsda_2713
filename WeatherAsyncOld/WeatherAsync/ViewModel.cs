﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WeatherAsync.Model;

namespace WeatherAsync
{
    class ViewModel : ViewModelBase
    {
        private string ip;
        public string Ip { get => ip; set => Set(ref ip, value); }

        private string country;
        public string Country { get => country; set => Set(ref country, value); }

        private string region;
        public string Region { get => region; set => Set(ref region, value); }

        private string city;
        public string City { get => city; set => Set(ref city, value); }

        private string temp;
        public string Temp { get => temp; set => Set(ref temp, value); }

        private string currency;
        public string Currency { get => currency; set => Set(ref currency, value); }

        private string image;
        public string Image { get => image; set => Set(ref image, value); }

        private string code;
        public string Code { get => code; set => Set(ref code, value); }

        private int progress;
        public int Progress { get => progress; set => Set(ref progress, value); }


        public Task FindIpInfo()
        {
            return Task.Run(() =>
            {
                WebClient webClient = new WebClient();
                var json = webClient.DownloadString("https://api.myip.com/");
                IpInfo data = JsonConvert.DeserializeObject<IpInfo>(json);
                Ip = data.Ip;
                Country = data.Country;
                Code = data.CC;
            });
        }

        public Task FindLocationInfo()
        {
            return Task.Run(() =>
            {
                WebClient webClient = new WebClient();
                var query = $"https://api.ipgeolocation.io/ipgeo?apiKey=8b630bdaffc04c57996f6cb6f0552f28&ip={Ip}";
                var json = webClient.DownloadString(query);
                LocationInfo data = JsonConvert.DeserializeObject<LocationInfo>(json);
                City = data.City;
                Region = data.State_prov;
            });
        }

        public Task FindWeatherInfo()
        {
            return Task.Run(() =>
            {
                WebClient webClient = new WebClient();
                var query = $"https://api.openweathermap.org/data/2.5/weather?q={City}&appid=835492c0eab300994ec658dfb16ad305&units=metric";
                var json = webClient.DownloadString(query);
                dynamic data = JsonConvert.DeserializeObject(json);
                Temp = data.main.temp;
            });
        }

        public Task FindCurrencyInfo()
        {
            return Task.Run(() =>
            {
                WebClient webClient = new WebClient();
                var query = $"https://restcountries.eu/rest/v2/name/{Country}";
                var json = webClient.DownloadString(query);
                dynamic data = JsonConvert.DeserializeObject(json);
                Currency = data[0].currencies[0].name;
                Image = $"https://www.countryflags.io/{Code}/shiny/64.png";
            });
        }

        public async void Search()
        {
            await FindIpInfo();
            Progress += 25;
            await FindLocationInfo();
            Progress += 25;
            await FindWeatherInfo();
            Progress += 25;
            await FindCurrencyInfo();
            Progress += 25;
        }

        private RelayCommand searchCommand;
        public RelayCommand SearchCommand
        {
            get => searchCommand ?? (searchCommand = new RelayCommand(
                () =>
                {
                    Search();
                }
            ));
        }
    }
}
