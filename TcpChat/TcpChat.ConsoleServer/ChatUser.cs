﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TcpChat.ConsoleServer
{
    class ChatUser
    {
        public string Username { get; set; }
        public TcpClient Client { get; set; }
        public StreamReader Reader { get; set; }
        public StreamWriter Writer { get; set; }

        public ChatUser(TcpClient client, string username)
        {
            Client = client;
            Username = username;
            Reader = new StreamReader(client.GetStream());
            Writer = new StreamWriter(client.GetStream());
            Writer.AutoFlush = true;
        }
    }
}
