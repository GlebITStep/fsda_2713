﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TcpChat.WpfClient
{
    class MainViewModel : ViewModelBase
    {
        private string username;
        public string Username { get => username; set => Set(ref username, value); }

        private string ip = "127.0.0.1";
        public string IP { get => ip; set => Set(ref ip, value); }

        private string message;
        public string Message { get => message; set => Set(ref message, value); }
        
        private int lastIndex = 0;
        public int LastIndex { get => lastIndex; set => Set(ref lastIndex, value); }

        private ObservableCollection<Message> messages = new ObservableCollection<Message>();
        public ObservableCollection<Message> Messages { get => messages; set => Set(ref messages, value); }

        TcpClient client;
        StreamWriter writer;
        StreamReader reader;

        public MainViewModel()
        {
            client = new TcpClient();
        }

        private RelayCommand connectCommand;
        public RelayCommand ConnectCommand
        {
            get => connectCommand ?? (connectCommand = new RelayCommand(
                () =>
                {
                    client.Connect(IPAddress.Parse(IP), 8080);
                    writer = new StreamWriter(client.GetStream());
                    reader = new StreamReader(client.GetStream());
                    writer.AutoFlush = true;
                    writer.WriteLine($"connect:{Username}");
                    ReceiveMessages(reader);
                }
            ));
        }

        private RelayCommand disconnectCommand;
        public RelayCommand DisconnectCommand
        {
            get => disconnectCommand ?? (disconnectCommand = new RelayCommand(
                () =>
                {
                    writer.WriteLine($"end:");
                    writer.Close();
                    reader.Close();
                    client.Close();
                }
            ));
        }


        private RelayCommand sendCommand;
        public RelayCommand SendCommand
        {
            get => sendCommand ?? (sendCommand = new RelayCommand(
                () =>
                {
                    writer.WriteLine($"message:{Message}");
                    Message = string.Empty;
                }
            ));
        }

        public void ReceiveMessages(StreamReader reader)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    var message = reader.ReadLine();
                    var user = message.Split(':')[0];
                    var text = message.Split(':')[1];
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        var msg = new Message { Username = user, Text = text };
                        msg.MyMessage = Username == user;
                        Messages.Add(msg);
                        LastIndex = Messages.Count - 1;
                    });
                }
            });
        }
    }
}
