﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpChat.WpfClient
{
    class Message
    {
        public string Username { get; set; }
        public string Text { get; set; }
        public bool MyMessage { get; set; }
    }
}
