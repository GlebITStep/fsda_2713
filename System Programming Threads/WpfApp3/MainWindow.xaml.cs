﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        //void CountWords()
        //{
        //    string filename = "";
        //    Dispatcher.Invoke(() => filename = FileName.Text);
        //    string text = File.ReadAllText(filename);
        //    var words = text.Split(' ');
        //    Dispatcher.Invoke(() => Result.Content = words.Count());
        //}

        ////void CountWords2(object filename)
        ////{
        ////    var text = File.ReadAllText(filename as string);
        ////    var words = text.Split(' ');
        ////    //MessageBox.Show(words.Count().ToString());
        ////    Dispatcher.Invoke(() => Result.Content = words.Count());
        ////}

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    Thread thread = new Thread(new ThreadStart(CountWords));
        //    thread.Start();

        //    //Thread thread = new Thread(new ParameterizedThreadStart(CountWords2));
        //    //thread.Start(FileName.Text);
        //}
    }
}
