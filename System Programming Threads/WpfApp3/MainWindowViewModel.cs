﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp3
{
    class MainWindowViewModel : ViewModelBase
    {
        private string fileName = @"C:\Skripnikov\Test.txt";
        public string FileName { get => fileName; set => Set(ref fileName, value); }

        private string resultFileName = @"C:\Skripnikov\copy.txt";
        public string ResultFileName { get => resultFileName; set => Set(ref resultFileName, value); }

        private int result = 0;
        public int Result { get => result; set => Set(ref result, value); }

        private double progress = 0;
        public double Progress { get => progress; set => Set(ref progress, value); }

        //void CountWords()
        //{
        //    string text = File.ReadAllText(FileName);
        //    var words = text.Split(' ');
        //    Result = words.Count();
        //}

        private RelayCommand countCommand;
        public RelayCommand CountCommand
        {
            get => countCommand ?? (countCommand = new RelayCommand(
                () =>
                {
                    //Thread thread = new Thread(new ThreadStart(CountWords));
                    //thread.Start();

                    ThreadPool.QueueUserWorkItem(param =>
                    {
                        string text = File.ReadAllText(FileName);
                        var words = text.Split(' ');
                        Result = words.Count();
                    });
                }
            ));
        }

        private RelayCommand copyCommand;
        public RelayCommand CopyCommand
        {
            get => copyCommand ?? (copyCommand = new RelayCommand(
                () =>
                {
                    ThreadPool.QueueUserWorkItem(param =>
                    {
                        long length = new FileInfo(FileName).Length;
                        long doneLength = 0;
                        using (var outputFile = new FileStream(ResultFileName, FileMode.OpenOrCreate))
                        {
                            using (var inputFile = new FileStream(FileName, FileMode.Open))
                            {
                                var buffer = new byte[100000];
                                int copied = 0;
                                while ((copied = inputFile.Read(buffer, 0, 100000)) > 0)
                                {
                                    doneLength += copied;
                                    outputFile.Write(buffer, 0, copied);
                                    Progress = (double)doneLength / (double)length * 100;
                                }
                            }
                        }
                        MessageBox.Show("Done!");


                        //var bytes = File.ReadAllBytes(FileName);
                        //File.WriteAllBytes(ResultFileName, bytes);
                        //MessageBox.Show("Done!");
                    });

                    //ThreadPool.QueueUserWorkItem(param =>
                    //{
                    //    var bytes = File.ReadAllBytes(FileName);
                    //    File.WriteAllBytes(ResultFileName, bytes);
                    //    MessageBox.Show("Done!");
                    //});
                }
            ));
        }
    }
}


