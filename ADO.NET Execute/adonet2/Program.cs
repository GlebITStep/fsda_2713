﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adonet2
{
    class Program
    {
        static void Main(string[] args)
        {
            //SelectStudents();
            //AddStudent("Iskender", "Iskender");
            Console.WriteLine(StudentsCount());
        }

        static int StudentsCount()
        {
            var connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (var connection = new SqlConnection(connString))
            {
                connection.Open();

                var query = "select count(*) from Students";
                var command = new SqlCommand(query, connection);
                var count = (int)command.ExecuteScalar();
                return count;
            }
        }

        static void AddStudent(string name, string surname, int groupId = 2, int term = 1)
        {
            var connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (var connection = new SqlConnection(connString))
            {
                connection.Open();

                try
                {
                    var query = $"insert into Students values (@name, @surname, @groupId, @term)";
                    var command = new SqlCommand(query, connection);
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@name",
                        Value = name
                    });
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@surname",
                        Value = surname
                    });
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@groupId",
                        Value = groupId
                    });
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@term",
                        Value = term
                    });
                    var result = command.ExecuteNonQuery(); //insert update delete
                    Console.WriteLine(result);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        static void SelectStudents()
        {
            var connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (var connection = new SqlConnection(connString))
            {
                connection.Open();

                var query = "select Id, FirstName, LastName from Students";
                var command = new SqlCommand(query, connection);
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Console.Write($"{reader.GetInt32(0)}\t");
                        Console.Write($"{reader.GetString(1)}\t");
                        Console.Write($"{reader.GetString(2)}\t");
                        Console.WriteLine();
                    }
                }
                else
                    Console.WriteLine("No results!");
            }
        }
    }
}
