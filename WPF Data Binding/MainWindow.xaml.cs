using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp12
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Test Data { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Data = new Test();

            DataContext = Data;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Data.Text = "Gleb!";
        }

        private void ButtonPlus(object sender, RoutedEventArgs e)
        {
            Data.Size++;
        }

        private void ButtonMinus(object sender, RoutedEventArgs e)
        {
            Data.Size--;
        }
    }

    public class Test : ObservableObject
    {
        private string text = "Test";
        public string Text
        {
            get => text;
            set => Set(ref text, value);
        }

        private string name = "Elvin";
        public string Name
        {
            get => name;
            set => Set(ref name, value);
        }

        private int size = 10;
        public int Size
        {
            get => size;
            set => Set(ref size, value);
        }
    }
}
