using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfApp12
{
    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        protected void Set<T>(ref T field, T value, [CallerMemberName]string prop = "")
        {
            field = value;
            OnPropertyChanged(prop);
        }
    }
}
