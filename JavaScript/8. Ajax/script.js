//http://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix

function searchMovies() {
	event.preventDefault();

	let name = document.forms.movieForm.movieName.value;
	console.log(name);

	let xhttp = new XMLHttpRequest();
	
	xhttp.onload = function() {
		let resultElem = document.querySelector("#result");

		let data = JSON.parse(xhttp.response);
		console.log(xhttp.response);

		for (const movie of data.Search) {
			// let template = `
			// <li>
			// 	<h1>${movie.Title}</h1>
			// 	<img src="${movie.Poster}" width="300" height="300">
			// </li>`;

			let template = `
			<div class="col-sm-3">
				<div class="card"">
					<img height="200" style="object-fit:cover" src="${movie.Poster}" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title text-truncate">${movie.Title}</h5>
						<a href="#" class="btn btn-primary">Details</a>
					</div>
				</div>
			</div>
			`;

			// resultElem.insertAdjacentHTML('beforeend', `<li>${movie.Title}</li>`)
			resultElem.insertAdjacentHTML('beforeend', template);		
			console.log(movie.Title);
		}
	}

	xhttp.open("GET", `http://www.omdbapi.com/?apikey=2c9d65d5&s=${name}`);
	xhttp.send();

	document.forms.movieForm.movieName.value = '';
}