let playerLeft = 300;
let playerTop = 300;
let rotation = 0;
let mouseX = 0;
let mouseY = 0;
let speed = 10;

function onKeyDown() {
    let player = document.querySelector('#player');

    switch (event.key) {
        case 'w':
            playerTop -= speed;
            break;
        case 'a':
            playerLeft -= speed;
            break;
        case 's':
            playerTop += speed;
            break;
        case 'd':
            playerLeft += speed;
            break;
        default:
            break;
    }

    player.style.left = `${playerLeft}px`;
    player.style.top = `${playerTop}px`;
}

function startGame() {
    let player = document.querySelector('#player');
    player.style.left = `${playerLeft}px`;
    player.style.top = `${playerTop}px`;
    let maxX = document.scrollingElement.clientWidth - 80;
    let maxY = document.scrollingElement.clientHeight - 80;

    for (let i = 0; i < 100; i++) {
        let food = document.createElement('div');
        food.className = "food";
        document.body.appendChild(food);
        food.style.left = `${rndNum(0, maxX)}px`;
        food.style.top = `${rndNum(0, maxY)}px`;

        // food.addEventListener("mouseover", function() {
        //     food.remove();
        // });  
    }
}

function rndNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

document.addEventListener("DOMContentLoaded", function() {

    document.addEventListener("keydown", onKeyDown);  

    startGame();

    let player = document.querySelector('#player');
    player.addEventListener("click", function() {
        rotation+=360;
        player.style.transition = "0.3s";
        player.style.transform = `rotate(${rotation}deg)`;
    });  

    // setInterval(function() {
    //     let foods = document.querySelectorAll('.food');
    //     foods.forEach(elem => {
    //         let maxX = document.scrollingElement.clientWidth - 80;
    //         let maxY = document.scrollingElement.clientHeight - 80;

    //         let cont = true;
    //         //console.log(maxX + " " + maxY);          
    //         do {
    //             let X = rndNum(0, maxX);
    //             let Y = rndNum(0, maxY);
    //             if ((X < playerLeft - 100 || X > playerLeft + 100) || (Y < playerTop - 100 || Y > playerTop + 100)) {
    //                 elem.style.left = `${X}px`;
    //                 elem.style.top = `${Y}px`;
    //                 elem.style.transform = `rotate(${rndNum(0, 720)}deg) scale(${Math.random() + 0.5})`;
    //                 cont = false;
    //             }
    //         } while (cont);
            

    //     });
    // }, 1000);


    setInterval(function() {
        let foods = document.querySelectorAll('.food');
        foods.forEach(elem => {
            let foodX = parseInt(window.getComputedStyle(elem).left);
            let foodY = parseInt(window.getComputedStyle(elem).top);
            let maxX = document.scrollingElement.clientWidth - 80;
            let maxY = document.scrollingElement.clientHeight - 80;
            let cont = true;      

            do {
                
                let X = rndNum(foodX, foodX + (Math.random() > 0.5 ? 100 : -100));
                let Y = rndNum(foodY, foodY + (Math.random() > 0.5 ? 100 : -100));
                if ((X < mouseX - 200 || X > mouseX + 200) || (Y < mouseY - 200 || Y > mouseY + 200)) {
                    elem.style.left = `${X}px`;
                    elem.style.top = `${Y}px`;
                    elem.style.transform = `rotate(${rndNum(0, 720)}deg) scale(${Math.random() + 0.5})`;
                    cont = false;
                } else {
                    foodX = rndNum(0, maxX);
                    foodY = rndNum(0, maxY);
                }
            } while (cont);

            // let X = rndNum(foodX, foodX + (Math.random() > 0.5 ? 50 : -50));
            // let Y = rndNum(foodY, foodY + (Math.random() > 0.5 ? 50 : -50));
            // elem.style.left = `${X}px`;
            // elem.style.top = `${Y}px`;
            // // elem.style.transform = `rotate(${rndNum(0, 720)}deg) scale(${Math.random() + 0.5})`;
            // elem.style.transform = `rotate(${rndNum(0, 720)}deg)`;
            // cont = false;
    

        });
    }, 500);

    document.addEventListener("mousemove", function(event) {
        mouseX = event.x;
        mouseY = event.y;
    });  
});