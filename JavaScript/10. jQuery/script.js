let contacts = [];

$(function(){	
	contacts = JSON.parse(localStorage.getItem('contacts'));
	console.log(contacts);
	
	if (contacts) {
		for (const contact of contacts) {
			drawContacts(contact);
		}
	} else {
		contacts = [];
	}
});

$('#clear').click(function() {
	localStorage.clear();
	location.reload();
});

function saveContact() {
	let name = $('#name').val();
	let photo = $('#photo').val();
	let info = $('#info').val();

	let contact = {
		name: name,
		photo: photo,
		info: info
	};

	contacts.push(contact);
	localStorage.setItem('contacts', JSON.stringify(contacts));
	console.log(contacts);

	$('#name').val('');
	$('#photo').val('');
	$('#info').val('');

	$('#addModal').modal('hide');

	drawContacts(contact);
}

function drawContacts(contact) {
	let template = `
		<a href="#" class="list-group-item list-group-item-action">
			<div class="d-flex w-100 justify-content-between">
				<h5 class="mb-1">${contact.name}</h5>
			</div>
			<p class="mb-1">${contact.info}</p>
		</a>
	`;
	$('#contactList').append(template);
}

