let player = {
    x: 0,
    y: 0,
    score: 0
}

let screenWidth = document.scrollingElement.clientWidth;
let screenHeight = document.scrollingElement.clientHeight;

document.addEventListener("DOMContentLoaded", function() {

    document.body.addEventListener("mousemove", function() {  
        draw();
        player.x = event.x;
        player.y = event.y;

        let foods = document.querySelectorAll('.food');
        for (const food of foods) {
            let foodX = parseInt(food.style.left);
            let foodY = parseInt(food.style.top);

            if (player.x > foodX - 0 - player.score * 0.01 && 
                player.x < foodX + 50 + player.score * 0.01 && 
                player.y > foodY - 0 - player.score * 0.01 && 
                player.y < foodY + 50 + player.score * 0.01) {
                food.remove();
                player.score += 100;
            }
            
        }
    });

    for (let i = 0; i < 100; i++) {
        let food = document.createElement('div');
        food.className = 'food';
        food.style.top = `${rndNum(0, screenWidth)}px`;
        food.style.left = `${rndNum(0, screenHeight)}px`;
        document.body.appendChild(food);
    }

});

function draw() {
    let elem = document.querySelector('#player');
    elem.style.top = `${player.y - 50}px`;
    elem.style.left = `${player.x - 50}px`;
    elem.style.transform = `scale(${1 + (player.score * 0.001)})`;

    let score = document.querySelector('#score');
    score.innerText = `Score: ${player.score}`;
    // let degree = rotation(event.x, event.y);
    // console.log(degree);    
    // elem.style.transform = `rotate(${degree}deg)`;
}

function rndNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function rotation(mouseX, mouseY) {
  
    var diffX = player.x - mouseX;
    var diffY = player.y - mouseY;
    var tan = diffY / diffX;

    var atan = Math.atan(tan) * 180 / Math.PI;;
    if(diffY > 0 && diffX > 0)
        atan += 180; 
    else if(diffY < 0 && diffX > 0)
        atan -= 180;

    return atan;
}