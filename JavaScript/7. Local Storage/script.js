let tasks = [];

document.addEventListener('DOMContentLoaded', function() {
	let myName = localStorage.getItem('name');
	if (myName) {
		let formNameElem = document.querySelector('#nameForm');
		formNameElem.setAttribute('hidden', 'true');

		let myNameElem = document.querySelector('#myNameHeader');
		myNameElem.children[0].innerText += myName;
		myNameElem.removeAttribute('hidden');
	}

	let jsonData = localStorage.getItem('tasks');
	if (jsonData) {
		tasks = JSON.parse(jsonData);
		// console.log(tasks);
		
		for (const task of tasks) {
			drawTask(task);
		}
	}

});

function addTask() {
	event.preventDefault();
		
	let elem = document.forms.addForm.taskName;
	let taskName = elem.value;

	{
		// if (taskName) {
		// 	let taskElem = document.createElement('li');
		// 	taskElem.innerText = taskName;
		// 	taskElem.className = 'list-group-item d-flex justify-content-between align-items-center';
		// 	taskElem.insertAdjacentHTML('beforeend', '<span class="badge badge-primary badge-pill"><i class="fas fa-times"></i></span>');

		// 	let listElem = document.querySelector('#taskList');
		// 	listElem.appendChild(taskElem);

		// 	document.forms.addForm.taskName.value = '';
		// }
	}

	if (taskName) {
		tasks.push(taskName);
		// console.log(tasks);	
		localStorage.setItem('tasks', JSON.stringify(tasks));
		document.forms.addForm.taskName.value = '';
		drawTask(taskName);
	}
}

function drawTask(name) {
	let template = `
	<li class="list-group-item d-flex justify-content-between align-items-center">
		${name}
		<span onclick="deleteTask()" class="badge badge-primary badge-pill"><i class="fas fa-times"></i></span>
	</li>`

	let listElem = document.querySelector('#taskList');
	listElem.insertAdjacentHTML('beforeend', template);
}

function deleteTask() {
	let elem = event.currentTarget.parentElement;
	let parent = elem.parentElement;
	
	let index = 0;
	for (const child of parent.children) {
		if (child == elem)
			break;	
		index++;
	}

	tasks.splice(index, 1);
	localStorage.setItem('tasks', JSON.stringify(tasks));

	elem.remove();
}

function addName() {
	let myName = document.forms.nameForm.myName.value;
	localStorage.setItem("name", myName);
}

function removeName() {
	localStorage.removeItem("name");
	location.reload();
}

// function saveTask() {
// 	localStorage.setItem()
// }