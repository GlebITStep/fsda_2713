﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TeamViewer.Client.ViewModels
{
    class ClientViewModel : ViewModelBase
    {
        private ImageSource image;
        public ImageSource Image { get => image; set => Set(ref image, value); }

        private int port = 8080;
        public int Port { get => port; set => Set(ref port, value); }

        private RelayCommand conenctCommand;
        public RelayCommand ConenctCommand
        {
            get => conenctCommand ?? (conenctCommand = new RelayCommand(
                () =>
                {
                    Task.Run(() =>
                    {

                        UdpClient udpClient = new UdpClient(Port);
                        using (var memory = new MemoryStream())
                        {
                            while (true)
                            {
                                try
                                {
                                    IPEndPoint endPoint = null;
                                    byte[] data = udpClient.Receive(ref endPoint);
                                    memory.Write(data, 0, data.Length);
                                    if (data.Length == 3 && data[0] == 1 && data[1] == 2 && data[2] == 3)
                                    {
                                        Application.Current.Dispatcher.Invoke(() =>
                                        {
                                            Image = ByteToImage(memory.GetBuffer());
                                            memory.SetLength(0);
                                        });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }

                        }


                    });
                }
            ));
        }

        public ImageSource ByteToImage(byte[] imageData)
        {
            try
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;

                return imgSrc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }

        }
    }
}
