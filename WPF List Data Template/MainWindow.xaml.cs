﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;

namespace WpfApp8
{
    public partial class MainWindow : Window
    {
        ObservableCollection<Person> list = new ObservableCollection<Person>();

        public MainWindow()
        {
            InitializeComponent();

            list.Add(new Person { Name = "Gleb", Surname = "Skripnikov" });
            list.Add(new Person { Name = "Murad", Surname = "Mammadli" });
            list.Add(new Person { Name = "Gleb3", Surname = "Skripnikov3" });

            listBox.ItemsSource = list;


            WebClient webClient = new WebClient();
            var title = "Terminator 2";
            var result = webClient.DownloadString($"http://www.omdbapi.com/?apikey=2c9d65d5&t={title}");
            dynamic data = JsonConvert.DeserializeObject(result);
            MessageBox.Show(data.Poster.ToString());
        }
    }

    class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
