﻿using Autofac;
using ContactListApp.Model;
using ContactListApp.Services;
using ContactListApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp
{
    class ViewModelLocator
    {
        public AppViewModel AppViewModel { get; set; }
        public ContactListViewModel ContactListViewModel { get; set; }
        public AddContactViewModel AddContactViewModel { get; set; }
        public ContactViewModel ContactViewModel { get; set; }

        public INavigationService NavigationService { get; set; }

        public ViewModelLocator()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<AppViewModel>();
            builder.RegisterType<ContactListViewModel>();
            builder.RegisterType<AddContactViewModel>();
            builder.RegisterType<ContactViewModel>();
            builder.RegisterType<NavigationService>().As<INavigationService>().InstancePerLifetimeScope();
            builder.RegisterType<ContactsRepository>().As<IContactsRepository>().InstancePerLifetimeScope();
            builder.RegisterType<CategoriesRepository>().As<ICategoriesRepository>().InstancePerLifetimeScope();
            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope())
            {
                AppViewModel = scope.Resolve<AppViewModel>();
                ContactListViewModel = scope.Resolve<ContactListViewModel>();
                AddContactViewModel = scope.Resolve<AddContactViewModel>();
                ContactViewModel = scope.Resolve<ContactViewModel>();

                NavigationService = scope.Resolve<INavigationService>();
            };

            NavigationService.AddPage("ContactList", ContactListViewModel);
            NavigationService.AddPage("AddContact", AddContactViewModel);
            NavigationService.AddPage("Contact", ContactViewModel);

            NavigationService.Navigate("ContactList");
        }
    }
}
