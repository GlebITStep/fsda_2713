﻿using ContactListApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Services
{
    interface ICategoriesRepository
    {
        void Insert(Category category);
        void Remove(Category category);
        void Remove(int id);
        IEnumerable<Category> Get();
    }
}
