﻿using ContactListApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContactListApp.Services
{
    class ContactsRepository : IContactsRepository
    {
        public ContactAppContext DB { get; set; }

        public ContactsRepository()
        {
            DB = new ContactAppContext();
        }

        public void Insert(Contact contact)
        {
            try
            {
                DB.Contacts.Add(contact);
                DB.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Remove(Contact contact)
        {
            DB.Contacts.Remove(contact);
            DB.SaveChanges();
        }

        public void Remove(int id)
        {
            DB.Contacts.Remove(DB.Contacts.Where(x => x.Id == id).FirstOrDefault());
            DB.SaveChanges();
        }

        public IEnumerable<Contact> Get()
        {
            return DB.Contacts;
        }
    }
}
