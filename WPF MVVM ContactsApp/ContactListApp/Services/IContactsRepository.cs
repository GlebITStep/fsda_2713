﻿using ContactListApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Services
{
    interface IContactsRepository
    {
        void Insert(Contact contact);
        void Remove(Contact contact);
        void Remove(int id);
        IEnumerable<Contact> Get();
    }
}
