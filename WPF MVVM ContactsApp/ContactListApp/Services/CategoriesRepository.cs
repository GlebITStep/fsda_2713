﻿using ContactListApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContactListApp.Services
{
    class CategoriesRepository : ICategoriesRepository
    {
        public ContactAppContext DB { get; set; }

        public CategoriesRepository()
        {
            DB = new ContactAppContext();
        }

        public void Insert(Category category)
        {
            try
            {
                DB.Categories.Add(category);
                DB.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Remove(Category category)
        {
            DB.Categories.Remove(category);
            DB.SaveChanges();
        }

        public void Remove(int id)
        {
            DB.Categories.Remove(DB.Categories.Where(x => x.Id == id).FirstOrDefault());
            DB.SaveChanges();
        }

        public IEnumerable<Category> Get()
        {
            return DB.Categories;
        }
    }
}
