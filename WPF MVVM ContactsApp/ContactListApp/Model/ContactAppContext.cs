﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Model
{
    class ContactAppContext : DbContext
    {
        static ContactAppContext()
        {
            Database.SetInitializer(new ContactsAppInitializer());
        }

        public ContactAppContext() : base("DefaultConnection") { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Contact> Contacts { get; set; }
    }
}
