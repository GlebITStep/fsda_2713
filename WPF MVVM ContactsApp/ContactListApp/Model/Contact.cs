﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Model
{
    class Contact
    {
        public int Id { get; set; }

        [MaxLength(50, ErrorMessage = "Name length can't be more than 50 symbols")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [MaxLength(50)]
        public string Surname { get; set; }

        [Range(10, 99)]
        public int? Age { get; set; }

        [MaxLength(20)]
        [Required]
        [Phone]
        public string Phone { get; set; }

        public bool? Gender { get; set; }

        public int? CategoryId { get; set; }

        //Navigation property
        public Category Category { get; set; } = new Category();
    }
}
