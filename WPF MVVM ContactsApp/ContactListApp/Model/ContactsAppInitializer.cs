﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Model
{
    class ContactsAppInitializer : CreateDatabaseIfNotExists<ContactAppContext>
    {
        protected override void Seed(ContactAppContext db)
        {
            db.Categories.Add(new Category { Name = "Family" });
            db.Categories.Add(new Category { Name = "Friends" });
            db.Categories.Add(new Category { Name = "Students" });
            db.Categories.Add(new Category { Name = "Co-workers" });
            db.SaveChanges();

            db.Contacts.Add(new Contact
            {
                Name = "Iskender",
                Surname = "Rajabli",
                Age = 18,
                Gender = true,
                Phone = "123456789",
                Category = db.Categories.Where(x => x.Name == "Students").FirstOrDefault()
            });
            db.Contacts.Add(new Contact
            {
                Name = "Murad",
                Surname = "Mammadli",
                Age = 15,
                Gender = true,
                Phone = "987654321",
                Category = db.Categories.Where(x => x.Name == "Students").FirstOrDefault()
            });
            db.Contacts.Add(new Contact
            {
                Name = "Gleb",
                Surname = "Skripnikov",
                Age = 24,
                Gender = true,
                Phone = "111222333",
                Category = db.Categories.Where(x => x.Name == "Family").FirstOrDefault()
            });
            db.SaveChanges();
        }
    }
}
