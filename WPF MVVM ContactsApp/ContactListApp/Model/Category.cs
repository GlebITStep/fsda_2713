﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Model
{
    class Category
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        //Navigation property
        public List<Contact> Contacts { get; set; } = new List<Contact>();
    }
}
