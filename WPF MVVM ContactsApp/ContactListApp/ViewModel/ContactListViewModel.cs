﻿using ContactListApp.Messages;
using ContactListApp.Model;
using ContactListApp.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContactListApp.ViewModel
{
    class ContactListViewModel : ViewModelBase
    {
        public INavigationService NavigationService { get; }
        public IContactsRepository ContactsRepository { get; }

        private ObservableCollection<Contact> contacts = new ObservableCollection<Contact>();
        public ObservableCollection<Contact> Contacts { get => contacts; set => Set(ref contacts, value); }

        public ContactListViewModel(INavigationService navigationService, IContactsRepository contactsRepository)
        {
            NavigationService = navigationService;
            ContactsRepository = contactsRepository;
            Contacts = new ObservableCollection<Contact>(ContactsRepository.Get());

            Messenger.Default.Register<AddEditContactMessage>(this, param =>
            {
                Contacts = new ObservableCollection<Contact>(ContactsRepository.Get());
            });
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get => addCommand ?? (addCommand = new RelayCommand(
                () => 
                {
                    Messenger.Default.Send(new AddEditContactMessage { Contact = new Contact() });
                    NavigationService.Navigate("AddContact");
                } 
            ));
        }

        private RelayCommand<Contact> openContactCommand;
        public RelayCommand<Contact> OpenContactCommand
        {
            get => openContactCommand ?? (openContactCommand = new RelayCommand<Contact>(
                param =>
                {
                    Messenger.Default.Send(new ViewContactMessage { Contact = param });
                    NavigationService.Navigate("Contact");
                }
            ));
        }

        private RelayCommand<Contact> deleteContactCommand;
        public RelayCommand<Contact> DeleteContactCommand
        {
            get => deleteContactCommand ?? (deleteContactCommand = new RelayCommand<Contact>(
                param =>
                {
                    ContactsRepository.Remove(param);
                    Contacts.Remove(param);
                    //Contacts = new ObservableCollection<Contact>(ContactsRepository.Get());
                }
            ));
        }

        private RelayCommand<Contact> editContactCommand;
        public RelayCommand<Contact> EditContactCommand
        {
            get => editContactCommand ?? (editContactCommand = new RelayCommand<Contact>(
                param =>
                {
                    Messenger.Default.Send(new AddEditContactMessage { Contact = param, IsEdit = true });
                    NavigationService.Navigate("AddContact");
                }
            ));
        }
    }
}
