﻿using ContactListApp.Messages;
using ContactListApp.Model;
using ContactListApp.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.ViewModel
{
    class ContactViewModel : ViewModelBase
    {
        public INavigationService NavigationService { get; }

        private Contact contact = new Contact();
        public Contact Contact { get => contact; set => Set(ref contact, value); }

        public ContactViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;

            Messenger.Default.Register<ViewContactMessage>(this, param =>
            {
                Contact = param.Contact;
            });
        }

        private RelayCommand goBackCommand;
        public RelayCommand GoBackCommand
        {
            get => goBackCommand ?? (goBackCommand = new RelayCommand(
                () =>
                {
                    NavigationService.Navigate("ContactList");
                }
            ));
        }

    }
}
