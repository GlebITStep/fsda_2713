﻿using ContactListApp.Messages;
using ContactListApp.Model;
using ContactListApp.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.ViewModel
{
    class AddContactViewModel : ViewModelBase
    {
        public INavigationService NavigationService { get; }
        public IContactsRepository ContactsRepository { get; }
        public ICategoriesRepository CategoriesRepository { get; }

        private Contact contact = new Contact();
        public Contact Contact { get => contact; set => Set(ref contact, value); }

        private ObservableCollection<Category> categories = new ObservableCollection<Category>();
        public ObservableCollection<Category> Categories { get => categories; set => Set(ref categories, value); }

        private bool isEdit;
        public bool IsEdit { get => isEdit; set => Set(ref isEdit, value); }

        public AddContactViewModel(
            INavigationService navigationService, 
            IContactsRepository contactsRepository, 
            ICategoriesRepository categoriesRepository)
        {
            NavigationService = navigationService;
            ContactsRepository = contactsRepository;
            CategoriesRepository = categoriesRepository;

            Categories = new ObservableCollection<Category>(CategoriesRepository.Get());

            Messenger.Default.Register<AddEditContactMessage>(this, param =>
            {
                Contact = param.Contact;
                IsEdit = param.IsEdit;
            });
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get => addCommand ?? (addCommand = new RelayCommand(
                () => 
                {
                    ContactsRepository.Insert(Contact);
                    Messenger.Default.Send(new AddEditContactMessage { Contact = Contact });
                    NavigationService.Navigate("ContactList");
                }
            ));
        }

        private RelayCommand editCommand;
        public RelayCommand EditCommand
        {
            get => editCommand ?? (editCommand = new RelayCommand(
                () =>
                {
                    //ContactsRepository.Insert(Contact);
                    //Messenger.Default.Send(new AddEditContactMessage { Contact = Contact });
                    //NavigationService.Navigate("ContactList");
                }
            ));
        }

        private RelayCommand goBackCommand;
        public RelayCommand GoBackCommand
        {
            get => goBackCommand ?? (goBackCommand = new RelayCommand(
                () =>
                {
                    NavigationService.Navigate("ContactList");
                }
            ));
        }
    }
}
