﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ContactListApp.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool data = (bool)value;
            if (data)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
            //if (value != null)
            //{
            //    int integer = (int)value;
            //    if (integer == int.Parse(parameter.ToString()))
            //        return true;
            //    else
            //        return false;
            //}
            //return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return parameter;
        }
    }
}
