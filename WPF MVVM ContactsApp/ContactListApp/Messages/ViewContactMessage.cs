﻿using ContactListApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Messages
{
    class ViewContactMessage
    {
        public Contact Contact { get; set; }
    }
}
