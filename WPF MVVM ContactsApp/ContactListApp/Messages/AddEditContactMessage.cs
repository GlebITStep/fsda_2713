﻿using ContactListApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Messages
{
    class AddEditContactMessage
    {
        public Contact Contact { get; set; }
        public bool IsEdit { get; set; } = false;
    }
}
