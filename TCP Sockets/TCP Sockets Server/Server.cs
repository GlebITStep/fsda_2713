﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCP_Sockets_Server
{
    class Server
    {
        static void Main(string[] args)
        {
            TcpListener server = new TcpListener(IPAddress.Any, 8080);
            server.Start();

            Console.WriteLine("Server started...");
            TcpClient connection = server.AcceptTcpClient();
            Console.WriteLine("Client connected");

            using (StreamReader reader = new StreamReader(connection.GetStream()))
            {
                while (true)
                {
                    var message = reader.ReadLine();
                    Console.WriteLine($"Message: {message}");
                }
            }
        }
    }


    //class Server
    //{
    //    static void Main(string[] args)
    //    {
    //        TcpListener server = new TcpListener(IPAddress.Any, 8080);
    //        server.Start();

    //        Console.WriteLine("Server started...");
    //        TcpClient connection = server.AcceptTcpClient();
    //        Console.WriteLine("Client connected");

    //        using (StreamReader reader = new StreamReader(connection.GetStream()))
    //        {
    //            while (true)
    //            {
    //                var message = reader.ReadLine();
    //                Console.WriteLine($"Message: {message}");
    //            }
    //        }
    //    }
    //}

    //class Server
    //{
    //    static void Main(string[] args)
    //    {
    //        IPEndPoint endPoint = new IPEndPoint(
    //            IPAddress.Parse("127.0.0.1"), 27020);

    //        Socket socket = new Socket(
    //            AddressFamily.InterNetwork,
    //            SocketType.Stream,
    //            ProtocolType.Tcp);

    //        socket.Bind(endPoint);

    //        socket.Listen(200);
    //        Console.WriteLine("Server started...");

    //        Socket handler = socket.Accept();

    //        Console.WriteLine("Client connected!");
    //        while (true)
    //        {
    //            byte[] bytes = new byte[1000];
    //            int count = handler.Receive(bytes);
    //            Console.WriteLine($"{count} bytes received");
    //            var message = Encoding.Unicode.GetString(bytes, 0, count);
    //            Console.WriteLine(message);
    //            Console.WriteLine("End!");
    //        }
    //    }
    //}
}
