﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCP_Sockets_Client
{
    class Client
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient();
            client.Connect("127.0.0.1", 8080);

            using (StreamWriter writer = new StreamWriter(client.GetStream()))
            {
                //writer.AutoFlush = true;
                while (true)
                {
                    Console.Write("Enter message: ");
                    var message = Console.ReadLine();
                    writer.WriteLine(message);
                    writer.Flush();
                }
            }
        }
    }

    //class Client
    //{
    //    static void Main(string[] args)
    //    {
    //        IPEndPoint endPoint = new IPEndPoint(
    //            IPAddress.Parse("127.0.0.1"), 27020);

    //        Socket socket = new Socket(
    //            AddressFamily.InterNetwork,
    //            SocketType.Stream,
    //            ProtocolType.Tcp);

    //        try
    //        {
    //            socket.Connect(endPoint);

    //            while (true)
    //            {
    //                Console.Write("Enter data: ");
    //                var message = Console.ReadLine();
    //                socket.Send(Encoding.Unicode.GetBytes(message));
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine(ex.Message);
    //        }

    //        Console.ReadKey();
    //    }
    //}
}
