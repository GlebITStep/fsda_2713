﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TODO.Model;
using TODO.Tools;

namespace TODO.ViewModel
{
    class ToDoViewModel : ObservableObject
    {
        private ObservableCollection<ToDo> list = new ObservableCollection<ToDo>();
        public ObservableCollection<ToDo> List { get => list; set => Set(ref list, value); }

        private ToDo selected;
        public ToDo Selected { get => selected; set => Set(ref selected, value); }

        private string input;
        public string Input { get => input; set => Set(ref input, value); }


        public ToDoViewModel()
        {
            List.Add(new ToDo { Title = "One", Done = false });
            List.Add(new ToDo { Title = "Two", Done = true });
            List.Add(new ToDo { Title = "Three", Done = false });
        }


        private RelayCommand addCommand;
        public RelayCommand AddCommand {
            get => addCommand ?? (addCommand = new RelayCommand(
                param =>
                {
                    List.Add(new ToDo { Title = Input, Done = false });
                    Input = "";
                },
                param => Input?.Length > 0
            ));
        }

        private RelayCommand deleteCommand;
        public RelayCommand DeleteCommand
        {
            get => deleteCommand ?? (deleteCommand = new RelayCommand(
                param => List.Remove(param as ToDo)
            ));
        }

        private RelayCommand editCommand;
        public RelayCommand EditCommand
        {
            get => editCommand ?? (editCommand = new RelayCommand(
                param =>
                {
                    var todo = param as ToDo;
                    todo.Done = !todo.Done;
                },
                param => Selected != null
            ));
        }
    }
}
