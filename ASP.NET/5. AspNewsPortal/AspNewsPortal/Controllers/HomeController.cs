﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspNewsPortal.Models;

namespace AspNewsPortal.Controllers
{
    public class HomeController : Controller
    {
        private readonly NewsDbContext context;

        public HomeController(NewsDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View(context.Posts.ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
