﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    public class HomeController : Controller
    {
        private readonly ToDoDbContext context;

        public HomeController(ToDoDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var result = context.ToDoItems.ToList();
            ViewBag.Name = "Gleb";
            ViewData["Surname"] = "Skripnikov";
            ViewBag.Items = result;

            return View(result);
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddTask(ToDoItem item)
        {
            context.ToDoItems.Add(item);
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult DeleteTask(int id)
        {
            var task = context.ToDoItems.FirstOrDefault(x => x.Id == id);
            context.ToDoItems.Remove(task);
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult CheckTask(int id)
        {
            var task = context.ToDoItems.FirstOrDefault(x => x.Id == id);
            task.IsDone = !task.IsDone;
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        //[HttpPost]
        //public IActionResult AddTask(string name, string description)
        //{
        //    context.ToDoItems.Add(new ToDoItem { Name = name, Description = description });
        //    context.SaveChanges();

        //    return RedirectToAction("Index");
        //    //return Content($"Name: {name}\nDescription: {description}");
        //}
    }
}