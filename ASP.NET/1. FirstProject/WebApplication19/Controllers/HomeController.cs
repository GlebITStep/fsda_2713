﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication19.Controllers
{
    public class HomeController : Controller
    {
        // /Home/Index
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AboutMe()
        {
            return View();
        }

        // /Home/String
        public IActionResult String()
        {
            return Content("Index");
        }

        // /Home/Json
        public IActionResult Json()
        {
            return Json( new { Name = "Gleb", Age = 25 } );
        }
    }
}