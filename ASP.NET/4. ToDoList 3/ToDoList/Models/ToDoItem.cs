﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoList.Models
{
    public class ToDoItem
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Gde Name?")]
        //[MinLength(5, ErrorMessage = "Che tak malo?")]
        //[RegularExpression("^[A-Z,А-Я][a-z,а-я, ]{4,}$", ErrorMessage = "Incorrect input!!!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Gde Description?")]
        //[MinLength(5, ErrorMessage = "Che tak malo?")]
        public string Description { get; set; }

        public bool IsDone { get; set; }

        public string Image { get; set; }
    }
}
