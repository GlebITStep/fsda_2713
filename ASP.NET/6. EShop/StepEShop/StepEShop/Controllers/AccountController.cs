﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace StepEShop.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;

        public AccountController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegisterUser(string username, string password)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = username,
                Email = username
            };

            var result = await userManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }

            TempData["Errors"] = "";
            foreach (var item in result.Errors)
            {
                TempData["Errors"] += item.Description + "\n";
            }
            
            return RedirectToAction("Register");
        }

        [HttpPost]
        public async Task<IActionResult> LoginUser(string username, string password)
        {
            var user = await userManager.FindByNameAsync(username);
            var result = await userManager.CheckPasswordAsync(user, password);
            if (result)
            {
                await signInManager.SignInAsync(user, false);
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Login");
        }

        public async Task<IActionResult> SignOut()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}