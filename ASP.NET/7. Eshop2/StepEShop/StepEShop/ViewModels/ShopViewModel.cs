﻿using StepEShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StepEShop.ViewModels
{
    public class ShopViewModel
    {
        public List<Product> Products{ get; set; }
        public List<CartItem> Cart { get; set; }
    }
}
