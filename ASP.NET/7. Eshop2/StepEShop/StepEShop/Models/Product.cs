﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StepEShop.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Range(0, 100000)]
        public int Price { get; set; }

        [Required]
        public string Image { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Range(0, 100)]
        public int Discount { get; set; }

        public bool New { get; set; }
    }
}
