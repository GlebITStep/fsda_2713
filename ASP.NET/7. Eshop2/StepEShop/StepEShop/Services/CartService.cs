﻿using StepEShop.Models;
using StepEShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StepEShop.Services
{
    public class CartService
    {
        public List<CartItem> Cart { get; set; } = new List<CartItem>();

        public void Add(Product product)
        {
            var cartItem = Cart.FirstOrDefault(x => x.Item.Id == product.Id);
            if (cartItem != null)
                cartItem.Quantity++;
            else
                Cart.Add(new CartItem { Item = product, Quantity = 1 });
        }

        public int CartCount()
        {
            return Cart.Sum(x => x.Quantity);
        }

        public double TotalPrice()
        {
            double totalPrice = 0;
            foreach (var item in Cart)
            {
                totalPrice += (item.Item.Price * item.Quantity) - item.Item.Price * item.Quantity / 100.0 * item.Item.Discount;
            }
            return totalPrice;
        }

        public double OldPrice()
        {
            double oldPrice = 0;
            foreach (var item in Cart)
            {
                oldPrice += item.Item.Price * item.Quantity;
            }
            return oldPrice;
        }
    }
}
