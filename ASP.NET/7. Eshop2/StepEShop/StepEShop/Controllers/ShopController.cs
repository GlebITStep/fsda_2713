﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StepEShop.Models;
using StepEShop.Services;
using StepEShop.ViewModels;

namespace StepEShop.Controllers
{
    public class ShopController : Controller
    {
        private readonly ShopDbContext context;
        private readonly CartService cartService;

        public ShopController(
            ShopDbContext context,
            CartService cartService)
        {
            this.context = context;
            this.cartService = cartService;
        }

        public IActionResult Index()
        {
            var products = context.Products.ToList();

            ViewBag.TotalPrice = cartService.TotalPrice();
            ViewBag.OldPrice = cartService.OldPrice();
            ViewBag.CartCount = cartService.CartCount();

            var data = new ShopViewModel { Products = products, Cart = cartService.Cart };
            return View(data);
        }

        public IActionResult AddToCart(int id)
        {
            var product = context.Products.FirstOrDefault(x => x.Id == id);
            cartService.Add(product);

            return RedirectToAction("Index");
        }
    }
}