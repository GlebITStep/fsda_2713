﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StepEShop.Models;

namespace StepEShop.Controllers
{
    public class ContactsController : Controller
    {
        private readonly ShopDbContext context;

        public ContactsController(ShopDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SendMessage(Message message)
        {
            context.Messages.Add(message);
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}