﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    public class HomeController : Controller
    {
        private readonly ToDoDbContext context;

        public HomeController(ToDoDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var result = context.ToDoItems.ToList();

            ViewBag.Done = result.Count(x => x.IsDone);
            ViewBag.Undone = result.Count(x => !x.IsDone);

            return View(result);
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddTask(ToDoItem item, IFormFile taskImage)
        {
            if (taskImage != null)
            {
                var extension = Path.GetExtension(taskImage.FileName);
                if (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".gif")
                {           
                    var filename = Guid.NewGuid().ToString() + extension;
                    var filePath = $@"{Directory.GetCurrentDirectory()}\wwwroot\images\{filename}";
                    using (var fs = new FileStream(filePath, FileMode.Create))
                    {
                        taskImage.CopyTo(fs);
                    }
                    item.Image = filename;
                }
                else
                {
                    return RedirectToAction("Add");
                }
            }

            if (ModelState.IsValid)
            {
                context.ToDoItems.Add(item);
                context.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Add");
        }

        public IActionResult DeleteTask(int id)
        {
            var task = context.ToDoItems.FirstOrDefault(x => x.Id == id);
            context.ToDoItems.Remove(task);
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult CheckTask(int id)
        {
            var task = context.ToDoItems.FirstOrDefault(x => x.Id == id);
            task.IsDone = !task.IsDone;
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        //[HttpPost]
        //public IActionResult AddTask(string name, string description)
        //{
        //    context.ToDoItems.Add(new ToDoItem { Name = name, Description = description });
        //    context.SaveChanges();

        //    return RedirectToAction("Index");
        //    //return Content($"Name: {name}\nDescription: {description}");
        //}
    }
}