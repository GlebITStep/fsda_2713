<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<form action="calc.php" method="get">
		<input type="number" name="num1" value="<?php echo $_GET['num1'] ?>">
		<select name="operation">
			<option value="sum">+</option>
			<option value="subs">-</option>
			<option value="mult">*</option>
			<option value="div">/</option>
		</select>
		<input type="number" name="num2" value="<?php echo $_GET['num2'] ?>">
		<span>= </span>
		<span>
			<?php
			if (isset($_GET['num1']) && isset($_GET['num2'])) {
				if ($_GET['operation'] == 'sum')
					echo $_GET['num1'] + $_GET['num2'];
				if ($_GET['operation'] == 'subs')
					echo $_GET['num1'] - $_GET['num2'];
				if ($_GET['operation'] == 'mult')
					echo $_GET['num1'] * $_GET['num2'];
				if ($_GET['operation'] == 'div')
					echo $_GET['num1'] / $_GET['num2'];
			} 
			else
				echo 0;
			?>
		</span>

		<button>Calc</button>
	</form>
</body>
</html>