<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
    <body>
    <h1>My first PHP Website!</h1>
    <?php 
        // // echo
        // echo '<h2>Hello, world!</h2>';
        // echo '<p>2 + 3 = ' . (2 + 3) . '</p>';
        // echo 'My name is ' . 'Gleb';


        // // Variables
        // $age = 25;
        // echo 'My age is: '.$age.'!';


        // // isset & unset
        // $age = 25;
        // echo $age;
        // unset($age);
        // if (isset($age)) 
        //     echo $age;
        // else
        //     echo "Error!";


        // // Variables 2
        // $name = 'Gleb';
        // echo $name;
        // $name = 25;
        // echo $name;


        // $var = 25;
        // echo gettype($var);
        // if (gettype($var) == 'integer')
        //     echo "yes";
        // else 
        //     echo "no"


        // $var = 25;
        // $var = $var . 'a';
        // echo $var;


        // $var = 25;
        // echo (is_integer($var) ? 'yes' : 'no') . '<br>';
        // echo (is_double($var) ? 'yes' : 'no') . '<br>';
        // echo (is_bool($var) ? 'yes' : 'no') . '<br>';
        // echo (is_string($var) ? 'yes' : 'no') . '<br>';
        // echo (is_array($var) ? 'yes' : 'no') . '<br>';
        // echo (is_object($var) ? 'yes' : 'no') . '<br>';
        // echo (is_numeric($var) ? 'yes' : 'no') . '<br>';


        // $pi = 3.14;
        // echo $pi . '<br>';
        // settype($pi, 'integer');
        // echo $pi . '<br>';


        // const PI = 3.14;
        // echo PI;
        // // PI = 3.18; // error


        // define('PI', 3.14);
        // echo PI;
        // // PI = 3.18; // error


        // // default constants
        // echo __DIR__ . '<br>';
        // echo __FILE__ . '<br>';
        // echo __LINE__ . '<br>';
        // echo __FUNCTION__ . '<br>';
        // echo __CLASS__ . '<br>';
        // echo __METHOD__ . '<br>';
        // echo __NAMESPACE__ . '<br>';


        // $age = 91;
        // if ($age >= 18 && $age < 90) {
        //     echo 'Welcome!';
        // } else if ($age < 18) {
        //     echo 'Get out!';
        // } else if ($age >= 90) {
        //     echo 'Welcome nemnojko!';
        // }
        

        // $var1 = true;
        // $var2 = false;
        // if ($var1 xor $var2) {
        //     echo 'yes';
        // }


        $array[0] = 11;
        $array[1] = 22;
        $array[2] = 33;
        $array[3] = 44;
        $array[4] = 55;
        // echo $array; // error
        print_r($array);
        echo '<br>';
        
        for ($i = 0; $i < 5; $i++) { 
            echo $array[$i] . ' ';
        }

        foreach ($array as $item) {
            echo $item . ' ';
        }
    ?>
    <h2>End!</h2>
</body>
</html>