<?php 
	session_start();
	
	if (isset($_POST['name'])) {
		$_SESSION['name'] = $_POST['name'];
	}

	if (isset($_POST['message'])) {
		//$_SESSION['message'] = $_POST['message'];
		array_push($_SESSION['messages'], $_POST['message']);
	}

	//$_SESSION['messages'] = array('Hello!', 'Test!', 'Hi!');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

	<div class="container">
		<?php if(!isset($_SESSION['name'])): ?>

			<h1>Hello, guest!</h2>
			<form action="session.php" method="post">
				Your name:
				<input type="text" name="name">
				<button>Submit</button>
			</form>

		<?php else: ?>

			<h1>Hello, <?php echo $_SESSION['name'] ?>!</h2>

			<?php foreach ($_SESSION['messages'] as $item): ?>

				<div class="alert alert-primary" role="alert">
					<?php echo $item ?>
				</div>

			<?php endforeach ?>

			<form action="session.php" method="post">
				<p>Your text:</p>
				<input type="text" name="message" style="width: 100%"/>
				<button>Send</button>
			</form>

		<?php endif ?>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>


<!-- 
<?php if ($var): ?>
		<h1 style="color: green">YES</h1>
	<?php else: ?>
		<h1 style="color: red">NO</h1>
<?php endif ?> 
-->


<!-- 
<?php if(isset($_SESSION['message'])): ?>

	<div class="alert alert-primary" role="alert">
		<?php echo $_SESSION['message']; ?>
	</div>

<?php endif ?> 
-->