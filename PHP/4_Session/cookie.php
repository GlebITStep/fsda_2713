<?php 
	if (isset($_POST['lang'])) {
		setcookie('lang', $_POST['lang'], time() + 3600, '/');
	}

	if (isset($_POST['color'])) {
		setcookie('color', $_POST['color'], time() + 3600, '/');
	}

	$text = array(
		'RU' => array('hello'=>'Привет', 'nice'=>'Приятно познакомиться!'),
		'EN' => array('hello'=>'Hello', 'nice'=>'Nice to meet you!'),
		'AZ' => array('hello'=>'Salam', 'nice'=>'Tanıştığınız üçün çox xoşdur!')
	);

	$lang = 'EN';
	if (isset($_COOKIE['lang'])) {
		$lang = $_COOKIE['lang'];
	}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<style>
		* {
			color: <?php echo (isset($_COOKIE['color']) ? $_COOKIE['color'] : 'black'); ?>
		}
	</style>

</head>
<body>
	<h1><?php echo $text[$lang]['hello'] ?>, Gleb!</h1>
	<h2><?php echo $text[$lang]['nice'] ?></h2>

	<h3>Time: <?php echo time() ?></h3>
	<h3>Date: <?php echo date('d-m-Y H:i:s') ?></h3>

	<form action="cookie.php" method="post">
		<span>Language:</span>
		<select name="lang">
			<option <?php echo ($lang == 'RU' ? 'selected' : '') ?> value="RU">Russian</option>
			<option <?php echo ($lang == 'EN' ? 'selected' : '') ?> value="EN">English</option>
			<option <?php echo ($lang == 'AZ' ? 'selected' : '') ?> value="AZ">Azerbaijanian</option>
		</select>
		<button>Change</button>
	</form>

	<form action="cookie.php" method="post">
		<span>Color:</span>
		<input type="color" name="color" value="<?php echo (isset($_COOKIE['color']) ? $_COOKIE['color'] : 'black'); ?>">
		<button>Change</button>
	</form>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>