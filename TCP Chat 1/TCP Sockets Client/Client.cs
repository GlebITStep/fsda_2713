﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCP_Sockets_Client
{
    class Client
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient();
            client.Connect("127.0.0.1", 8080);
            Console.Write("Enter your name: ");
            var username = Console.ReadLine();

            using (var writer = new StreamWriter(client.GetStream()))
            {
                writer.AutoFlush = true;
                writer.WriteLine($"connect:{username}");
                while (true)
                {
                    Console.Write("Enter message: ");
                    var key = Console.ReadKey(true);
                    if (key.Key == ConsoleKey.Escape)
                    { 
                        writer.WriteLine($"end:");
                        break;
                    }
                    var message = Console.ReadLine();
                    writer.WriteLine($"message:{message}");
                }
            }
            Console.WriteLine();
        }
    }
}
