﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCP_Sockets_Server
{
    //connect
    //message
    //end

    //connect:Gleb
    //message:Hello!
    //end:

    class Server
    {
        static void Main(string[] args)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, 27020);
            listener.Start();

            Console.WriteLine("Server started...");

            while (true)
            {
                var client = listener.AcceptTcpClient();
                ListenClient(client);
            }
        }

        static public void ListenClient(TcpClient client)
        {
            Task.Run(() =>
            {
                string username = null;
                bool connected = true;

                using (var reader = new StreamReader(client.GetStream()))
                {
                    while (connected)
                    {
                        var data = reader.ReadLine(); //connect:Iskender
                        var command = data.Split(':')[0];
                        var message = data.Split(':')[1];
                        switch (command)
                        {
                            case "connect":
                                username = message;
                                Console.WriteLine($"{username} connected!");
                                break;
                            case "message":
                                Console.WriteLine($"{username}: {message}");
                                break;
                            case "end":
                                Console.WriteLine($"{username} disconnected!");
                                connected = false;
                                break;
                        }
                    }
                    client.Close();
                }
            });
        }
    }
}
