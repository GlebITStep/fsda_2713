﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adonet
{
    class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public override string ToString() => $"{Id} {Name} {Surname}";
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>();

            var connectionString =
                ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var query = "select Id, FirstName, LastName from Students";
                var command = new SqlCommand(query, connection);
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var student = new Student();

                        student.Id = reader.GetInt32(0);
                        student.Name = reader.GetString(1);
                        student.Surname = reader.GetString(2);

                        students.Add(student);
                    }

                    foreach (var item in students)
                    {
                        Console.WriteLine(item);
                    }
                }
                else
                    Console.WriteLine("No results!");
            }





            //        var connectionString =
            //ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            //        using (var connection = new SqlConnection(connectionString))
            //        {
            //            connection.Open();

            //            var query = "select Id, FirstName, LastName from Students";
            //            var command = new SqlCommand(query, connection);
            //            var reader = command.ExecuteReader();
            //            if (reader.HasRows)
            //            {
            //                for (int i = 0; i < reader.FieldCount; i++)
            //                {
            //                    Console.Write($"{reader.GetName(i)}\t\t");
            //                }
            //                Console.WriteLine();

            //                while (reader.Read())
            //                {
            //                    for (int i = 0; i < reader.FieldCount; i++)
            //                    {
            //                        Console.Write($"{reader.GetValue(i)}\t\t");
            //                    }
            //                    Console.WriteLine();
            //                }
            //            }
            //            else
            //                Console.WriteLine("No results!");
            //        }









            //var connectionString = 
            //    ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;           

            //using (var connection = new SqlConnection(connectionString))
            //{
            //    connection.Open();

            //    var query = "select * from Books";
            //    var command = new SqlCommand(query, connection);
            //    var reader = command.ExecuteReader();
            //    if (reader.HasRows)
            //    {
            //        Console.WriteLine($"{reader.GetName(1)}:");

            //        while (reader.Read())
            //        {
            //            Console.WriteLine(reader.GetValue(1));
            //        }
            //    }
            //    else
            //        Console.WriteLine("No results!");
            //}






            //string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=C:\SKRIPNIKOV\LIBRARY\LIBRARYSQL.MDF;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //SqlConnection connection = new SqlConnection(connectionString);
            //try
            //{
            //    connection.Open();
            //    Console.WriteLine("Connected!");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //connection.Close();
        }
    }
}
