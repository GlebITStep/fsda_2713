import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Places';
  // latitude: 40.4148917
  // longitude: 49.8549291

  lat: number = 40.4148917;
  lng: number = 49.8549291;
}
