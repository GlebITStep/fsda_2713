import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.scss']
})
export class MovieSearchComponent implements OnInit {
  apiUrl = 'http://www.omdbapi.com';
  apiKey = '2c9d65d5';
  movieName: string;
  movieList: Array<any>;
  selectedMovie: any = null;
  fullInfo: any;

  constructor(private httpClient: HttpClient) { 
  }

  ngOnInit() { }

  async onSearch() {
    let response = await this.httpClient.get<any>(`${this.apiUrl}?apikey=${this.apiKey}&s=${this.movieName}`).toPromise();
    console.log(response); 
    this.movieList = response.Search; 
    this.movieName = '';
  }

  async onMovieSelect(movie) {
    this.selectedMovie = movie;
    let response = await this.httpClient.get<any>(`${this.apiUrl}?apikey=${this.apiKey}&i=${movie.imdbID}`).toPromise();
    this.fullInfo = response;
  }
}
