import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  tasks: Array<Task> = [];
  newTaskName: string = '';

  addNewTask() {
    if (this.newTaskName != '') {
      let task = new Task();
      task.name = this.newTaskName;
      task.isDone = false;
  
      this.tasks.push(task);
      this.newTaskName = ''; 
    }
  }

  changeTask(task) {
    task.isDone = !task.isDone;
  }

  deleteTask(task) {
    this.tasks = this.tasks.filter(x => x.name != task.name);
  }
}

class Task {
  name: string;
  isDone: boolean;
}
