class Person { 
    name: string;
    age: number;

    async sayHello() { 
        console.log(`Hello from ${this.name}`);
    }
}


// let myName: string;
// myName = 'Gleb';
// console.log(`My name is ${myName}`);