import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/models/contact';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
 
  contactList: Array<Contact>;
  selectedContact: Contact;
  newContact: Contact = new Contact();

  constructor() {
    this.contactList = [];
    // this.contactList.push({
    //   id: 1,
    //   name: 'Gleb',
    //   photoUrl: 'https://pp.userapi.com/c837323/v837323067/333f2/Dq1nj5d-LXI.jpg',
    //   surname: 'Skripnikov',
    //   phone: '1727351289',
    //   isFavorite: true
    // });
    // this.contactList.push({
    //   id: 2,
    //   name: 'Iskender',
    //   photoUrl: 'https://pp.userapi.com/c824504/v824504385/14d4a2/M5ZeaA4UcTA.jpg?ava=1',
    //   surname: 'Racabli',
    //   phone: '876587587',
    //   isFavorite: true
    // });
  }

  ngOnInit() {
    let json = localStorage.getItem('contacts');
    this.contactList = JSON.parse(json);
  }

  onContactClick(item: Contact) {
    this.selectedContact = item;
  }

  onFavoriteClick(item: Contact) {
    item.isFavorite = !item.isFavorite;

    localStorage.setItem('contacts', JSON.stringify(this.contactList));
  }

  onAddContact() {
    this.contactList.push(this.newContact);
    this.newContact = new Contact();

    localStorage.setItem('contacts', JSON.stringify(this.contactList));
  }

}
