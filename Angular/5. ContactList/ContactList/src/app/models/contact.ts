export class Contact {
	id: number;
	name: string;
	surname: string;
	photoUrl: string;
	phone: string;
	isFavorite: boolean = false;
}