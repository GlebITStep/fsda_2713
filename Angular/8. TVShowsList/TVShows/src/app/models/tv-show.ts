export class TvShow {
	id?: number;
	title?: string;
	season?: number;
	episode?: number;
	rating?: number;
	isDone?: boolean;
}
