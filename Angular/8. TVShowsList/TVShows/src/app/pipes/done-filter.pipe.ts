import { Pipe, PipeTransform } from '@angular/core';
import { TvShow } from '../models/tv-show';

@Pipe({
  name: 'doneFilter'
})
export class DoneFilterPipe implements PipeTransform {

  transform(value: Array<TvShow>, condidion?: boolean): Array<TvShow> {
    return value.filter(x => x.isDone == condidion);
  }

}
