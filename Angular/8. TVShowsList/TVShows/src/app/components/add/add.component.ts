import { Component, OnInit } from '@angular/core';
import { TvShow } from 'src/app/models/tv-show';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  newShow: TvShow = {};
  errorMessage: string = null;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
  }

  async onSubmit() {
    try {
      await this.httpClient.post('http://localhost:60565/api/tvshows', this.newShow).toPromise(); 
    } catch (error) {
      console.log(error); 
      this.errorMessage = error.error.Title[0];
    }
    this.newShow = {};
  }

}
