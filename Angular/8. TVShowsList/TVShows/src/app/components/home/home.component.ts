import { Component, OnInit } from '@angular/core';
import { TvShow } from 'src/app/models/tv-show';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  tvShows: Array<TvShow> = [];

  constructor(private httpClient: HttpClient) {}

  async ngOnInit() {
    let result = await this.httpClient.get<Array<TvShow>>('http://localhost:60565/api/tvshows').toPromise();
    this.tvShows = result;
  }

  async onDelete(id: number) {
    console.log(id);
    try {
      await this.httpClient.delete(`http://localhost:60565/api/tvshows/${id}`).toPromise();

      let result = await this.httpClient.get<Array<TvShow>>('http://localhost:60565/api/tvshows').toPromise();
      this.tvShows = result;
    } catch (error) {
      console.log(error);
    }
  }
}
