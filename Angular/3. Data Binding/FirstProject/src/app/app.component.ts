import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  name = 'Gleb';
  symbol: '+';
  number1: number = 7;
  number2: number = 5;
  result: number = 0;

  onOperationSelect() {
    if (this.symbol === '+') {
      this.result = this.number1 + this.number2;
    } else if  (this.symbol === '-') {
      this.result = this.number1 - this.number2;
    } else if  (this.symbol === '*') {
      this.result = this.number1 * this.number2;
    } else if  (this.symbol === '/') {
      this.result = this.number1 / this.number2;
    }
  }


  // onOperationClick(operation) {
  //   if (operation === '+') {
  //     this.result = this.number1 + this.number2;
  //   } else if  (operation === '-') {
  //     this.result = this.number1 - this.number2;
  //   } else if  (operation === '*') {
  //     this.result = this.number1 * this.number2;
  //   } else if  (operation === '/') {
  //     this.result = this.number1 / this.number2;
  //   }
  // }

}
